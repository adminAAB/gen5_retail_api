<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Inquiry Policy_API</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>fd78f7e2-56e8-4ac0-a003-9a6816fd2ca5</testSuiteGuid>
   <testCaseLink>
      <guid>a80c30a8-8b30-48bb-8cf6-75cd7e110544</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/9. Summary Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f9d8448-2fd7-4903-b8d5-f1feb0206c00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/10. Customer Category Personal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>34019a7f-d497-46d9-ba2d-946fcc71a258</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/11. Customer Category Company</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2d93c0d-3d22-4fd4-8e4e-f28b3fcd1db3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/12. Summary Info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>965c1c26-5dce-4590-b6ae-c60d6e634f60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/13. Workflow History</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d93315d-08ea-447c-aca6-7af20dae3da8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/14. Delivery History</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d46d94d9-3d57-4fe5-8f1d-958b866dd6e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/15. Payment History</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78f8d7e0-b7b1-4a89-aeff-445c2d3ea544</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/16. Claim History</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ee565b6-f20e-43f2-8f7d-192d1371e981</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/17. Survey Image</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>635ff8e5-9676-4ccb-893a-9f34fc890ddb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/1. Inquiry Search Polis Prokhus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3cd3d1d4-9bbb-4496-890a-34941054808b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/2. Inquiry Search OrderNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e510a504-14ef-4aa3-9804-9d77d6d78abb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/3. Inquiry Search RegNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5055afec-1493-498f-83c2-df94be8c2df0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/4. Inqury Search ContractNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dee85bd3-a0e0-4c63-8471-3d7d48be3193</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/5. Inquiry Search LKno</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a32e11ae-f643-4627-a911-044f44743875</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/6. Inquiry Search EngNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a566c329-ae2e-4395-9223-47b62f280929</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/7. Inquiry Search ChasNo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>48abe319-8d5e-48df-9f34-4e6d87ae0da0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/8. Inquiry Search SPKno</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd3699f7-78dc-4933-a786-c693a3e6a2b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/18. Inquiry Search Polis TI</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd60b21b-90fb-4e3f-8b1a-8efcc8eae508</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/19. Inquiry Search Polis Titan</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c43ea8e4-6b64-458a-9e33-3ca7ab9fe1e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/20. Inquiry Search Polis WIC</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1f9448e9-436e-4a23-b4ff-b95f2565639a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/21. Inquiry Search Polis Lexus</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>adf8a89e-c07d-4ad3-bbbe-9f8fd01149f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/22. Inquiry Search Polis Godig</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3aeb01ff-0156-41cc-b993-3e00540170d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/23. Inquiry Search Polis 2W</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a71a5ee-7e03-4aed-8f81-0a7140741d20</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/24. Inquiry Search Cancellation Polis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b49c8014-8d99-48bc-b52d-e5610dca78fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/25. Inquiry Search Endorsment Polis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e2030bf-797f-4378-a858-8b8a5ed4f469</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/26. Inquiry Search Renewal Policy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7a1e5fa6-60e3-42f2-b5bc-ade34182ba54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/27. Inquiry Search polis awal punya survey image</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>62055bcb-1463-4b90-bd4c-7d6fa4858a25</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/28. Inquiry Search polis awal punya tidak survey image</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a4fa53e-617c-46fe-ae78-7387da4947c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/29. Inquiry Search polis renewal punya survey image</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95392c1c-12e3-426d-8b6c-44b167b3b8ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Skenario API/Inquiry Policy/30. Inquiry Search polis renewal tidak punya survey image</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
