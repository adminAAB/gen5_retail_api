import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

else {
	String QueryMappingMaster = "select * from mapping_master_claim where Master_No = '" + Master + "'"
	ArrayList getNomorSPK = API.getOneColumnDatabase("172.16.94.74", "AAB", QueryMappingMaster, 'Claim_No')
   
	for (int i = 0 ; i < getNomorSPK.size() ; i++) {
		String displayParam = "Address,Claim_No,Estimated_Amount,Is_Online_Workshop,Loss_Date,Online_Workshop_Flag,Online_Workshop_Status,Pay_to_code,Policy_No,Report_No,spk_No,Settled_Status,Survey_No,Type_Loss_Id,workshop_code,isAutoApp,isB2B,Policy_Id"
		String QueryClaim = "SELECT " + displayParam + " FROM claim WHERE Claim_No = '" + getNomorSPK[i] + "' order by EntryDt ASC"
		ArrayList getClaimData = API.getOneRowDatabase("172.16.94.74", "AAB", QueryClaim)
	   
		if (getClaimData[6] == '') {
			getClaimData.set(6, '0')
		}
	   
		String QueryWorkshop = "select * from mst_workshop where workshop_id= (SELECT Workshop_Code From AAB.dbo.Claim WHERE Claim_No = '" + getNomorSPK[i] + "')"
		String getWorkshopName = API.getValueDatabase("172.16.94.74", "CPS", QueryWorkshop, "Workshop_Name")
	   
		String QueryCustomer = "select * from mst_customer where cust_id= (SELECT Pay_to_code From dbo.Claim WHERE Claim_No = '" + getNomorSPK[i] + "')"
		String getCustName = API.getValueDatabase("172.16.94.74", "AAB", QueryCustomer, "Name")
	   
		String queryworkshopCode = "SELECT * From dbo.Claim WHERE Claim_No = '" + getNomorSPK[i] + "'"
		String existWorkshop = API.getValueDatabase("172.16.94.74", "AAB", queryworkshopCode, "workshop_code")
	   
		String PaytoCode = null
	   
		if (existWorkshop != '') {
			PaytoCode = getWorkshopName
		} else {
			PaytoCode = getCustName
		}
	   
		if (getWorkshopName == 'null') {
			getWorkshopName = '-'
		}
	   
		ArrayList getDtlMV = API.getOneRowDatabase("172.16.94.74", "AAB", QueryDtlMV.replace("_SPKNo", getNomorSPK[i]))
	   
		String TypeLoss = API.getValueDatabase("172.16.94.74", "AAB", "select * from mst_general where type ='TOL' and code = '" + getClaimData[13] + "'", "description")
	   
		String PolicyHolder = API.getValueDatabase("172.16.94.74", "AAB", "select * from policy where policy_No = '" + getClaimData[8] + "' order by entryDt desc", "Policy_Holder_Code")
	   
		String Is2Wheeler = 0
		String wheeler = API.getValueDatabase("172.16.94.74", "AAB", "select * from mst_general where code = (select Vehicle_Type from dtl_mv where Policy_id = '" + getClaimData[17] + "')", "type")
	   
		if (wheeler == '2WV') {
			Is2Wheeler = 1
		}
	   
		String TPSubrogation = "Tidak"
		String TPL = API.getValueDatabase("172.16.94.74", "AAB", "select * from TPL_Subrogation where report_no = '" + getClaimData[9] + "'", "Report_No")
	   
		if (TPL != 'null') {
			TPSubrogation = "Ya"
		}
	   
		int CountRow = i + 1
		API.Note ("Check Data Row " + CountRow)
	   
		API.CompareResponseData(getClaimData[0], getResponse.data[i].Address, "Address - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getClaimData[1], getResponse.data[i].Claim_No, "Claim_No - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getClaimData[2], getResponse.data[i].EstimatedAmount, "Estimated Amount - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getClaimData[3], getResponse.data[i].Is_Online_Workshop, "Is_Online_Workshop - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getClaimData[4], (getResponse.data[i].Loss_Date).replace("T"," ").replace("Z",".0"), "Loss Date - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getClaimData[5], getResponse.data[i].Online_Workshop_Flag, "Online_Workshop_Flag - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getClaimData[6], getResponse.data[i].Online_Workshop_Status, "Online_Workshop_Status - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getClaimData[7], getResponse.data[i].Pay_To_Code, "Pay_To_Code - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getClaimData[8], getResponse.data[i].Policy_No, "Policy_No - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getClaimData[9], getResponse.data[i].Report_No, "Report_No - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getClaimData[10], getResponse.data[i].SPK_No, "SPK_No - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getClaimData[11], getResponse.data[i].Settled_Status, "Settled_Status - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getClaimData[12], getResponse.data[i].Survey_No, "Survey_No - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getClaimData[13], getResponse.data[i].Type_Loss_Id, "Type_Loss_Id - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getClaimData[14], getResponse.data[i].Workshop_Code, "Workshop_Code - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getClaimData[15], getResponse.data[i].isAutoApp, "isAutoApp - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getClaimData[16], getResponse.data[i].isB2B, "isB2B - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(PaytoCode, getResponse.data[i].Pay_To_Name, "Pay_To_Name - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getWorkshopName, getResponse.data[i].Workshop_Name, "Workshop_Name - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getDtlMV[1], getResponse.data[i].Chasis_Number, "Chasis_Number - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getDtlMV[2], getResponse.data[i].Registration_Number, "Registration_Number - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(getDtlMV[0], getResponse.data[i].Description, "Vehicle Name - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(TypeLoss, getResponse.data[i].Type_Loss, "Type_Loss - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(PolicyHolder, getResponse.data[i].Policy_Holder_Code, "Policy_Holder_Code - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(Is2Wheeler, getResponse.data[i].Is_2Wheeler_Vehicle, "Is_2Wheeler_Vehicle - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	   
		API.CompareResponseData(TPSubrogation, getResponse.data[i].TPL, "TPL - get_master_spk", FailureHandling.CONTINUE_ON_FAILURE)
	}