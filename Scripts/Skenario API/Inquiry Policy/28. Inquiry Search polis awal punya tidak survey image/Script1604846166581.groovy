import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.keyword.UI as UI

WebUI.callTestCase(findTestCase('API/Login/Signin'),
	[('strPassword') : GlobalVariable.password,
	 ('strUsername') : 'doc'])

String chasisno = API.getValueDatabase("172.16.94.74", "AAB", "select top 10 a.policy_no, a.survey_no, mv.chasis_number, pl.policy_no AS Old_Policy_No, pl.survey_no AS Old_Survey_No from policy as a inner join mst_order as b on a.policy_no=b.policy_no inner join dtl_mv mv ON a.policy_id=mv.policy_id left join survey as c on a.survey_no=c.survey_no and c.survey_type ='svplcy' inner join policy pl on b.old_policy_no=pl.policy_no left join survey as sv on sv.survey_no=pl.survey_no and sv.survey_type ='svplcy' where b.order_type='2' and a.survey_no <> '' and pl.survey_no = '' and pl.entrydt > '2019-01-01'", "chasis_number")
println(chasisno)
WebUI.callTestCase(findTestCase('API/Inquiry Polis/Inquiry Polis'),
	[('strPolicyNo') : '',
		('strOrderNo'): "",
		('strRegNo') : "",
		('strContractNo'):"",
		('strLKno'):"",
		('strEngNo') :"",
		('strChasNo'):chasisno,
		('strSPKno'):""])
