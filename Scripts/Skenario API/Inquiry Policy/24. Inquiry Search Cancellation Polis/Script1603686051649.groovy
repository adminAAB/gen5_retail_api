import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.API as API
import com.keyword.UI as UI

WebUI.callTestCase(findTestCase('API/Login/Signin'), 
	[('strPassword') : GlobalVariable.password, 
	 ('strUsername') : 'doc'])

String policyno = API.getValueDatabase("172.16.94.74", "AAB", "select top 1 * from policy where status='C' order by entrydt desc", "policy_no")
println(policyno)
WebUI.callTestCase(findTestCase('API/Inquiry Polis/Inquiry Polis'), 
	[('strPolicyNo') : policyno,
		('strOrderNo'): "",
		('strRegNo') : "",
		('strContractNo'):"",
		('strLKno'):"",
		('strEngNo') :"",
		('strChasNo'):"",
		('strSPKno'):""])

