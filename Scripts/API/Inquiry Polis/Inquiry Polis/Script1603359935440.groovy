import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.API
import com.keyword.UI

def inquiry = WS.sendRequest(findTestObject('Object Repository/Inquiry Policy/GetPolicyList'
	,[('token') : GlobalVariable.token,
		('PolicyNo') : strPolicyNo,
		('OrderNo') : strOrderNo,
		('RegNo') : strRegNo,
		('ContractNo') : strContractNo,
		('LKno') : strLKno,
		('EngNo'): strEngNo,
		('ChasNo'): strChasNo,
		('SPKno'):strSPKno]))

API.Note(inquiry.getStatusCode())
API.Note(API.getResponseData(inquiry))
if (inquiry.getStatusCode() == 200) {
	if (API.getResponseData(inquiry).Status == true && API.getResponseData(inquiry).Message == 'OK') {
	API.Note('Login Sukses')
	GlobalVariable.token = API.getResponseData(inquiry).token
} else if (API.getResponseData(inquiry).Status == false && API.getResponseData(inquiry).Message == 'Error') {
	API.Note('Login gagal')
} else if (API.getResponseData(inquiry).Status == false && API.getResponseData(inquiry).Error == 'a2is.Retail.TrackPolicy.API.Error') {
	API.Note('balikannya eror')
} else if (API.getResponseData(inquiry).Status == true && API.getResponseData(inquiry).TotalPages == 0) {
	API.Note('Data Not Found')
}

else {
	API.Note("Error Skenario !")
}
} else {
API.Note("Terjadi kesalahan pada website ! " + inquiry.getStatusCode())
}

//compare data yg di input
if(strPolicyNo == (API.getResponseData(inquiry).Data[0].PolicyNo).toString().trim()){
	API.Note('No polis SESUAI')
} else if(strOrderNo == (API.getResponseData(inquiry).Data[0].PolicyOrderNo).toString().trim()){
	API.Note('no order SESUAI')
} else if (strRegNo == (API.getResponseData(inquiry).Data[0].RegistrationNo).toString().trim()){
	API.Note('reg no SESUAI')
} else if (strContractNo == (API.getResponseData(inquiry).Data[0].ContractNo).toString().trim()){
	API.Note('Contract no SESUAI')
} else if (strEngNo == (API.getResponseData(inquiry).Data[0].EngineNo).toString().trim()){
	API.Note('Engine no SESUAI')
} else if (strChasNo == (API.getResponseData(inquiry).Data[0].ChassisNo).toString().trim()){
	API.Note('Chasis no SESUAI')
}
else {
	API.Note('DATA TIDAK SESUAI')
}

API.Note((API.getResponseData(inquiry).Data[0].PolicyNo).toString().trim())
API.Note((API.getResponseData(inquiry).Data[0].PolicyOrderNo).toString().trim())
API.Note((API.getResponseData(inquiry).Data[0].RegistrationNo).toString().trim())
API.Note((API.getResponseData(inquiry).Data[0].ContractNo).toString().trim())
API.Note((API.getResponseData(inquiry).Data[0].EngineNo).toString().trim())
API.Note((API.getResponseData(inquiry).Data[0].ChassisNo).toString().trim())



