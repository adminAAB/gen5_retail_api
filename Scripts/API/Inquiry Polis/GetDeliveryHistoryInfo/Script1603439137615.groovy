import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.API

def deliveryhistory = WS.sendRequest(findTestObject('Object Repository/Inquiry Policy/GetDeliveryHistoryInfo',
	[('PolicyNo') : strPolicyNo,
		('token') : GlobalVariable.token]))

API.Note(deliveryhistory.getStatusCode())
API.Note(API.getResponseData(deliveryhistory))
if (deliveryhistory.getStatusCode() == 200) {
	if (API.getResponseData(deliveryhistory).Status == true && API.getResponseData(deliveryhistory).Message == 'OK') {
	API.Note('Login Sukses')
	GlobalVariable.token = API.getResponseData(deliveryhistory).token
} else if (API.getResponseData(deliveryhistory).Status == false && API.getResponseData(deliveryhistory).Message == 'Error') {
	API.Note('Login gagal')
} else if (API.getResponseData(deliveryhistory).Status == false && API.getResponseData(deliveryhistory).Error == 'a2is.Retail.TrackPolicy.API.Error') {
API.Note('balikannya eror')
}
else {
	API.Note("Error Skenario !")
}
} else {
API.Note("Terjadi kesalahan pada website ! " + deliveryhistory.getStatusCode())
}