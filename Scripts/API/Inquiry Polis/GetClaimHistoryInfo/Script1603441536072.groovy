import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.API

def claimhistory = WS.sendRequest(findTestObject('Object Repository/Inquiry Policy/GetClaimHistoryInfo',
	[('PolicyNo') : strPolicyNo,
		('token') : GlobalVariable.token]))

API.Note(claimhistory.getStatusCode())
API.Note(API.getResponseData(claimhistory))
if (claimhistory.getStatusCode() == 200) {
	if (API.getResponseData(claimhistory).Status == true && API.getResponseData(claimhistory).Message == 'OK') {
	API.Note('Login Sukses')
	GlobalVariable.token = API.getResponseData(claimhistory).token
} else if (API.getResponseData(claimhistory).Status == false && API.getResponseData(claimhistory).Message == 'Error') {
	API.Note('Login gagal')
} else if (API.getResponseData(claimhistory).Status == false && API.getResponseData(claimhistory).Error == 'a2is.Retail.TrackPolicy.API.Error') {
API.Note('balikannya eror')
}
else {
	API.Note("Error Skenario !")
}
} else {
API.Note("Terjadi kesalahan pada website ! " + claimhistory.getStatusCode())
}