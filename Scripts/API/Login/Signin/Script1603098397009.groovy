import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

import com.keyword.API

def login = WS.sendRequest(findTestObject('Object Repository/Login/Login',
	[('username') : strUsername,
	 ('password') : strPassword]))

if (login.getStatusCode() == 200) {
	//pengecekkan login sukses
	if (API.getResponseData(login).status == true && API.getResponseData(login).errorCode == 0) {
		API.Note('Login Sukses')
		GlobalVariable.token = API.getResponseData(login).token
	} else if (API.getResponseData(login).status == false && API.getResponseData(login).errorCode == 100006) {
		API.Note('Login gagal')
		
	} else {
		API.Note("Error Skenario !")
	}
} else {
	API.Note("Terjadi kesalahan pada website ! " + login.getStatusCode())
}