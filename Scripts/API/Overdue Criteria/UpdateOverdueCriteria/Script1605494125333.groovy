import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.API
import com.keyword.UI

def UpdateOverdueCriteria = WS.sendRequest(findTestObject('Object Repository/Overdue Criteria/UpdateOverdueCriteria'
	,[('token') : GlobalVariable.token,
	 ('ESTONL') : intESTONL]))

Object GetResponse = API.getResponseData(UpdateOverdueCriteria)

API.Note(UpdateOverdueCriteria.getStatusCode())

API.Note(API.getResponseData(UpdateOverdueCriteria))

if (UpdateOverdueCriteria.getStatusCode() == 200) {
	if (GetResponse.status == true && GetResponse.message == 'Function OK!') {
	API.Note('Login Sukses')
	GlobalVariable.token = API.getResponseData(UpdateOverdueCriteria).token
} else if (GetResponse.status == false && GetResponse.message == 'Error') {
	API.Note('Login gagal')
} else if (GetResponse.status == false && GetResponse.Error == 'a2is.Retail.TrackPolicy.API.Error') {
	API.Note('balikannya eror')
}

else {
	API.Note("Error Skenario !")
}
} else {
API.Note("Terjadi kesalahan pada website ! " + UpdateOverdueCriteria.getStatusCode())
}


