import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.keyword.API
import com.keyword.UI

def overdue =WS.sendRequest(findTestObject('Object Repository/Overdue Criteria/GetOverdueCriteria' ,
	[('token') : GlobalVariable.token,
		]))

Object GetResponse = API.getResponseData(overdue)

//pengecekan api
API.Note(overdue.getStatusCode())
API.Note(API.getResponseData(overdue))
if (overdue.getStatusCode() == 200) {
	if (GetResponse.status == true && GetResponse.message == 'OK') {
	API.Note('Login Sukses')
	GlobalVariable.token = API.getResponseData(overdue).token
} else if (GetResponse.status == false && GetResponse.message == 'Error') {
	API.Note('Login gagal')
} else if (GetResponse.status == false && GetResponse.Error == 'a2is.Retail.TrackPolicy.API.Error') {
	API.Note('balikannya eror')
} else if (GetResponse.status == true && GetResponse.TotalPages == 0) {
	API.Note('Data Not Found')
}

else {
	API.Note("Error Skenario !")
}
} else {
API.Note("Terjadi kesalahan pada website ! " + overdue.getStatusCode())
}

//pengecekan db
String QueryAging = "select OverdueAging, DocumentCode, DocumentDescription from Claim_Overdue_Criteria"
ArrayList getAging = API.getOneColumnDatabase("172.16.94.74", "AAB", QueryAging, 'OverdueAging')
String QueryDocumentCode = "select * from Claim_Overdue_Criteria"

API.Note(getAging)
ArrayList getAllData = API.getAllDataDatabase("172.16.94.74", "AAB", QueryAging)
for(int a = 0 ; a < getAging.size() ; a++){
	API.CompareResponseData(getAllData[a][0],GetResponse.data[a].Aging, 'AgingOverdue - GetOverdueCriteria', FailureHandling.CONTINUE_ON_FAILURE)
	API.CompareResponseData(getAllData[a][1],GetResponse.data[a].DocumentCode, 'DocumentCode - GetOverdueCriteria', FailureHandling.CONTINUE_ON_FAILURE)
	API.CompareResponseData(getAllData[a][2],GetResponse.data[a].DocumentDescription, 'DocumentDescription - GetOverdueCriteria', FailureHandling.CONTINUE_ON_FAILURE)	
}




















