<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>UpdateOverdueCriteria</name>
   <tag></tag>
   <elementGuidId>ccd38219-76d2-4a3d-8df1-6d09df0efd86</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;OverdueValue&quot;,
      &quot;value&quot;: &quot;[{\&quot;DocumentCode\&quot;:\&quot;ESTONL\&quot;,\&quot;OverdueValue\&quot;:\&quot;${intESTONL}\&quot;},{\&quot;DocumentCode\&quot;:\&quot;ESTOFF\&quot;,\&quot;OverdueValue\&quot;:\&quot;0\&quot;},{\&quot;DocumentCode\&quot;:\&quot;INVONL\&quot;,\&quot;OverdueValue\&quot;:\&quot;7\&quot;},{\&quot;DocumentCode\&quot;:\&quot;INVOFF\&quot;,\&quot;OverdueValue\&quot;:\&quot;3\&quot;},{\&quot;DocumentCode\&quot;:\&quot;ESTTOTLOS\&quot;,\&quot;OverdueValue\&quot;:\&quot;3\&quot;},{\&quot;DocumentCode\&quot;:\&quot;INVTOTLOS\&quot;,\&quot;OverdueValue\&quot;:\&quot;3\&quot;},{\&quot;DocumentCode\&quot;:\&quot;PKB\&quot;,\&quot;OverdueValue\&quot;:\&quot;1\&quot;},{\&quot;DocumentCode\&quot;:\&quot;CLSETINVONL\&quot;,\&quot;OverdueValue\&quot;:\&quot;2\&quot;},{\&quot;DocumentCode\&quot;:\&quot;CLSETINVOFF\&quot;,\&quot;OverdueValue\&quot;:\&quot;3\&quot;},{\&quot;DocumentCode\&quot;:\&quot;PRINTVCHONL\&quot;,\&quot;OverdueValue\&quot;:\&quot;1\&quot;},{\&quot;DocumentCode\&quot;:\&quot;PRINTVCHOFF\&quot;,\&quot;OverdueValue\&quot;:\&quot;1\&quot;},{\&quot;DocumentCode\&quot;:\&quot;DOCRECEIVER\&quot;,\&quot;OverdueValue\&quot;:\&quot;1\&quot;}]&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${token}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Cookie</name>
      <type>Main</type>
      <value>${Cookie}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://gen5-qc.asuransiastra.com/retail/API/OverdueCriteria/UpdateOverdueCriteria</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
