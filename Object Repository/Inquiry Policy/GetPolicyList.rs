<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>GetPolicyList</name>
   <tag></tag>
   <elementGuidId>902c17b8-6530-4d8c-8e0d-8da6b36374bd</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;searchPolicyParameter&quot;,
      &quot;value&quot;: &quot;{\&quot;PageNo\&quot;:1,\&quot;PageSize\&quot;:10,\&quot;SortOption\&quot;:-1,\&quot;SortDirection\&quot;:12,\&quot;PolicyNo\&quot;:\&quot;${PolicyNo}\&quot;,\&quot;OrderNo\&quot;:\&quot;${OrderNo}\&quot;,\&quot;ChassisNo\&quot;:\&quot;${ChasNo}\&quot;,\&quot;EngineNo\&quot;:\&quot;${EngNo}\&quot;,\&quot;RegistrationNo\&quot;:\&quot;${RegNo}\&quot;,\&quot;CustomerID\&quot;:\&quot;\&quot;,\&quot;ContractNo\&quot;:\&quot;${ContractNo}\&quot;,\&quot;VANumber\&quot;:\&quot;\&quot;,\&quot;LKNumber\&quot;:\&quot;${LKno}\&quot;,\&quot;SPKNumber\&quot;:\&quot;${SPKno}\&quot;,\&quot;GroupNoteNumber\&quot;:\&quot;\&quot;}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer ${token}</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Cookie</name>
      <type>Main</type>
      <value>EGRUM_BTM=eeda96ff-ceb0-4d96-97c9-7d47b664e244#~#1||0; TS0125c461=015294299a35c2566fc45513b88e9ec631b873bc19711f56807e2ccc54563eae4ca3f84f371541e896100c8cc839067a933598a5af73c1ad29b42fb2d86897ac3607e5daa6</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://gen5-qc.asuransiastra.com/retail/API/InquiryPolicy/GetPolicyList</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
